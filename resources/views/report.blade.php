

                 <table class="table table-bordered table-hover">
                     <thead>
                                               
                     <tr>
                                <th colspan="{{count($toReturn['date_range']) + 7}}" style="text-align:center;"><?php echo 'Kelas : '.$toReturn['class']['className'].', Subyek : '.$toReturn['subject']['subjectTitle'] ?></th>
                            </tr>
                            <tr>
                                <th colspan="{{count($toReturn['date_range']) + 7}}" style="text-align:center;">P : Datang - A : Tidak hadir - L : Terlambat - E : Terlambat dengan alasan - D : Pulang awal</th>
                            </tr>
                    </thead>  
                        <tbody>
                            
                            <tr>
                                <th style="width: 10px">NISN     </th>
                                <th>Nama Siswa</th>
                                <th colspan="5">Persentase</th>
                                <th colspan="{{count($toReturn['date_range'])}}">Keterangan</th>
                            </tr>
                            <tr>
                                <td style="width: 10px"></td>
                                <td></td>
                                <td class="att_perc">
                                    <span tooltip>%P</span>
                                </td>
                                <td class="att_perc">
                                    <span tooltip >%A</span>
                                </td>
                                <td class="att_perc">
                                    <span tooltip >%L</span>
                                </td>
                                <td class="att_perc">
                                    <span tooltip>%E</span>
                                </td>
                                <td class="att_perc">
                                    <span tooltip >%D</span>
                                </td>
                                @foreach($toReturn['date_range'] as $value)
                                <td ng-repeat="range_one in date_range" style="text-align: center;">
                                    <span tooltip>{{$value['date']}}</span>
                                </td>
                                @endforeach
                            </tr>
                            <tr >
                            @foreach($toReturn['students'] as $value)
                                <td>{{ $value['studentRollId']}}</td>
                                <td style="white-space: nowrap;">
                                    {{$value['fullName']}}
                                </td>


                                <td class="att_perc">{{(int) $value['precentage'][1] }} %</td>
                                <td class="att_perc">{{ (int) $value['precentage'][0]}}  %</td>
                                <td class="att_perc">{{(int) $value['precentage'][2]}}  %</td>
                                <td class="att_perc">{{(int) $value['precentage'][3]}}  %</td>
                                <td class="att_perc">{{(int) $value['precentage'][4]}} %</td>
                                @foreach($toReturn['date_range'] as $values)
                                <td>

                                    @if(array_key_exists (strtotime($values['date']),$value['attendance']))

                                        <span style="text-align: center;">
                                            <span >

                                                <span>
                                                    @if($value['attendance'][strtotime($values['date'])]['status'] == 1)
                                                    <span  style="text-align: center;" ng-switch-when="1">P</span>
                                                    @elseif($value['attendance'][strtotime($values['date'])]['status']== 0)
                                                    <span style="text-align: center;" ng-switch-when="0">A</span>
                                                    @elseif($value['attendance'][strtotime($values['date'])]['status'] == 2)

                                                    <span style="text-align: center;" ng-switch-when="2">L</span>
                                                    @elseif($value['attendance'][strtotime($values['date'])]['status']== 3)

                                                    <span style="text-align: center;" ng-switch-when="3">E</span>
                                                    @elseif($value['attendance'][strtotime($values['date'])]['status'] == 4)

                                                    <span style="text-align: center;" ng-switch-when="4">D</span>
                                                    @else
                                                    <span>-</span>

                                                    @endif
                                                </span>

                                            </span>
                                        </span>
                                        
                                     @else
                                     <span>
                                            <span >

                                                <span>
                                                    <span>-</span>
                                                </span>

                                            </span>
                                        </span>
                                    @endif
                                </td>
                                @endforeach
                            </tr>
                            @endforeach

                        </tbody>
        </table>
   